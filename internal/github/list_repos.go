package github

import (
	"context"

	"github.com/shurcooL/githubv4"
	"gitlab.com/tanna.dev/renovate-config-sqlite/internal/domain"
)

func GetReposForOrg(ctx context.Context, client *githubv4.Client, org string) ([]domain.RepoKey, error) {
	var q struct {
		Organization struct {
			Repositories struct {
				Nodes []struct {
					Name       string
					IsArchived bool
				}
				PageInfo struct {
					EndCursor   githubv4.String
					HasNextPage bool
				}
			} `graphql:"repositories(first: 100, orderBy: {field: NAME, direction: ASC}, after: $cursor)"`
		} `graphql:"organization(login: $organization)"`
	}
	variables := map[string]interface{}{
		"organization": githubv4.String(org),
		"cursor":       (*githubv4.String)(nil),
	}

	var allRepos []domain.RepoKey
	for {
		err := client.Query(ctx, &q, variables)
		if err != nil {
			return nil, err
		}

		for _, n := range q.Organization.Repositories.Nodes {
			if n.IsArchived {
				continue
			}

			// allRepos = append(allRepos, fmt.Sprintf("%s/%s", org, n.Name))
			allRepos = append(allRepos, domain.RepoKey{
				Platform:     "github",
				Organisation: org,
				Repo:         n.Name,
			})
		}

		if !q.Organization.Repositories.PageInfo.HasNextPage {
			break
		}
		variables["cursor"] = githubv4.NewString(q.Organization.Repositories.PageInfo.EndCursor)
	}

	return allRepos, nil
}
