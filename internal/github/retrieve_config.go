package github

import (
	"context"
	"fmt"
	"net/http"

	"github.com/google/go-github/v61/github"
	"github.com/shurcooL/githubv4"
	"gitlab.com/tanna.dev/renovate-config-sqlite/internal/domain"
)

func RetrieveRenovateConfiguration(ctx context.Context, gqlClient *githubv4.Client, organisation, repo string) (*domain.Contents, error) {
	type fileContents struct {
		Blob struct {
			Text string
		} `graphql:"... on Blob"`
	}

	var q struct {
		Repository struct {
			// via https://docs.renovatebot.com/configuration-options/
			RenovateJSON           fileContents `graphql:"renovateJSON: object(expression: \"HEAD:renovate.json\")"`
			RenovateJSON5          fileContents `graphql:"renovateJSON5: object(expression: \"HEAD:renovate.json5\")"`
			DotGitHubRenovateJSON  fileContents `graphql:"dotGitHubRenovateJSON: object(expression: \"HEAD:.github/renovate.json\")"`
			DotGitHubRenovateJSON5 fileContents `graphql:"dotGitHubRenovateJSON5: object(expression: \"HEAD:.github/renovate.json5\")"`
			DotGitLabRenovateJSON  fileContents `graphql:"dotGitLabRenovateJSON: object(expression: \"HEAD:.gitlab/renovate.json\")"`
			DotGitLabRenovateJSON5 fileContents `graphql:"dotGitLabRenovateJSON5: object(expression: \"HEAD:.gitlab/renovate.json5\")"`
			DotRenovateRC          fileContents `graphql:"dotRenovateRC: object(expression: \"HEAD:.renovaterc\")"`
			DotRenovateRCJSON      fileContents `graphql:"dotRenovateRCJSON: object(expression: \"HEAD:.renovaterc.json\")"`
			DotRenovateRCJSON5     fileContents `graphql:"dotRenovateRCJSON5: object(expression: \"HEAD:.renovaterc.json5\")"`
		} `graphql:"repository(owner: $organization, name: $repo)"`
	}

	variables := map[string]interface{}{
		"organization": githubv4.String(organisation),
		"repo":         githubv4.String(repo),
	}

	err := gqlClient.Query(ctx, &q, variables)
	if err != nil {
		return nil, err
	}

	c := domain.Contents{
		Key: domain.RepoKey{
			Platform:     "github",
			Organisation: organisation,
			Repo:         repo,
		},
	}

	if q.Repository.RenovateJSON.Blob.Text != "" {
		c.ConfigPath = "renovate.json"
		c.RawConfig = q.Repository.RenovateJSON.Blob.Text
		return &c, nil
	}
	if q.Repository.RenovateJSON5.Blob.Text != "" {
		c.ConfigPath = "renovate.json5"
		c.RawConfig = q.Repository.RenovateJSON5.Blob.Text
		return &c, nil
	}
	if q.Repository.DotGitHubRenovateJSON.Blob.Text != "" {
		c.ConfigPath = ".github/renovate.json"
		c.RawConfig = q.Repository.DotGitHubRenovateJSON.Blob.Text
		return &c, nil
	}
	if q.Repository.DotGitHubRenovateJSON5.Blob.Text != "" {
		c.ConfigPath = ".github/renovate.json5"
		c.RawConfig = q.Repository.DotGitHubRenovateJSON5.Blob.Text
		return &c, nil
	}
	if q.Repository.DotGitLabRenovateJSON.Blob.Text != "" {
		c.ConfigPath = ".gitlab/renovate.json"
		c.RawConfig = q.Repository.DotGitLabRenovateJSON.Blob.Text
		return &c, nil
	}
	if q.Repository.DotGitLabRenovateJSON5.Blob.Text != "" {
		c.ConfigPath = ".gitlab/renovate.json5"
		c.RawConfig = q.Repository.DotGitLabRenovateJSON5.Blob.Text
		return &c, nil
	}
	if q.Repository.DotRenovateRC.Blob.Text != "" {
		c.ConfigPath = ".renovaterc"
		c.RawConfig = q.Repository.DotRenovateRC.Blob.Text
		return &c, nil
	}
	if q.Repository.DotRenovateRCJSON.Blob.Text != "" {
		c.ConfigPath = ".renovaterc.json"
		c.RawConfig = q.Repository.DotRenovateRCJSON.Blob.Text
		return &c, nil
	}
	if q.Repository.DotRenovateRCJSON5.Blob.Text != "" {
		c.ConfigPath = ".renovaterc.json5"
		c.RawConfig = q.Repository.DotRenovateRCJSON5.Blob.Text
		return &c, nil
	}
	if q.Repository.RenovateJSON.Blob.Text != "" {
		c.ConfigPath = "renovate.json"
		c.RawConfig = q.Repository.RenovateJSON.Blob.Text
		return &c, nil
	}

	return nil, nil
}

func RetrieveRenovateConfigurationPath(ctx context.Context, client *github.Client, owner, repo string, path string) (*domain.Contents, error) {
	f, _, resp, err := client.Repositories.GetContents(ctx, owner, repo, path, nil)
	if resp.StatusCode == http.StatusNotFound {
		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("failed to retrieve %v from %s/%s: %v", path, owner, repo, err)
	}

	if resp.StatusCode == http.StatusOK {
		c := domain.Contents{
			Key: domain.RepoKey{
				Platform:     "github",
				Organisation: owner,
				Repo:         repo,
			},
			ConfigPath: f.GetPath(),
		}
		c.RawConfig, err = f.GetContent()
		if err != nil {
			return nil, fmt.Errorf("failed to decode the contents of %v (which was returned successfully with encoding %v) from %s/%s: %v", path, f.GetEncoding(), owner, repo, err)
		}

		return &c, nil
	}

	return nil, nil
}
