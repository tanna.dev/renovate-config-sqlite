-- name: InsertRow :exec
INSERT INTO renovate_configs (
  platform,
  organisation,
  repo,

  config_path,
  config
) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?
);
