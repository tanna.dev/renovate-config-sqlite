-- renovate_configs contains the Renovate configuration files that have been
-- detected for given source forges (i.e. Github)
create table if not exists renovate_configs (
	-- platform is the source forge that this configuration is for, i.e. `github`
	platform text NOT NULL,
	-- organisation is the organisation on the source forge  that this
	-- configuration is for, i.e. `oapi-codegen`. May contain multiple `/`s on
	-- source forges that support it
	organisation text NOT NULL,
	-- repo is the owning repo
	repo text NOT NULL,

	-- config_path is the path within the repo that the Renovate configuraiton
	-- was found
	config_path text NOT NULL,
	-- config is the raw Renovate configuration that was found
	config text NOT NULL,

	UNIQUE (platform, organisation, repo) ON CONFLICT REPLACE
);

