package domain

type RepoKey struct {
	Platform     string
	Organisation string
	Repo         string
}

type Contents struct {
	Key        RepoKey
	ConfigPath string
	RawConfig  string
}
