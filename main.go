package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/bradleyfalzon/ghinstallation/v2"
	"github.com/google/go-github/v61/github"
	"github.com/shurcooL/githubv4"
	"github.com/urfave/cli/v2"
	"golang.org/x/oauth2"
	_ "modernc.org/sqlite"

	"github.com/jedib0t/go-pretty/v6/progress"
	"gitlab.com/tanna.dev/renovate-config-sqlite/internal/db"
	"gitlab.com/tanna.dev/renovate-config-sqlite/internal/domain"
	gh "gitlab.com/tanna.dev/renovate-config-sqlite/internal/github"
)

func main() {
	dbPathFlag := cli.StringFlag{
		Name:     "db",
		Usage:    "The path to the database",
		Required: true,
	}

	app := &cli.App{
		Usage: "Take Renovate configuration and collect it into an SQLite database",
		Commands: []*cli.Command{
			{
				Name:  "db",
				Usage: "Manage the SQLite database",
				Subcommands: []*cli.Command{
					{
						Name:  "init",
						Usage: "Prepare the SQLite database",
						Flags: []cli.Flag{
							&dbPathFlag,
						},
						Action: func(cCtx *cli.Context) error {
							return handleDBInit(cCtx.Context, cCtx.String("db"))
						},
					},
				},
			},
			{
				Name:  "discover",
				Usage: "Discover Renovate configuration in locations in the source forge",
				Subcommands: []*cli.Command{
					{
						Name:  "github",
						Usage: "Discover Renovate configuration in locations in GitHub",
						Flags: []cli.Flag{
							&dbPathFlag,
							&cli.StringFlag{
								Name:     "organisation",
								Usage:    "The GitHub organisation to scan",
								Required: true,
							},
						},
						Action: func(cCtx *cli.Context) error {
							return handleDiscoverGitHub(cCtx.Context, cCtx.String("db"), cCtx.String("organisation"))
						},
					},
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func newGitHubClients(ctx context.Context, githubToken string, ghAppID string, ghAppKeyPEM string, ghAppInstallationID string) (*github.Client, *githubv4.Client, error) {
	ghAppKeyPEM = strings.ReplaceAll(ghAppKeyPEM, "\\n", "\n")

	var httpClient *http.Client

	if ghAppID != "" && ghAppKeyPEM != "" {
		appID, err := strconv.ParseInt(ghAppID, 10, 64)
		if err != nil {
			return nil, nil, fmt.Errorf("failed to parse GITHUB_APP_ID (%v) as an int64: %v", ghAppID, err)
		}
		appInstallationID, err := strconv.ParseInt(ghAppInstallationID, 10, 64)
		if err != nil {
			return nil, nil, fmt.Errorf("failed to parse GITHUB_APP_INSTALLATION_ID (%v) as an int64: %v", ghAppInstallationID, err)
		}

		itr, err := ghinstallation.New(http.DefaultTransport, appID, appInstallationID, []byte(ghAppKeyPEM))
		if err != nil {
			return nil, nil, fmt.Errorf("failed to create GitHub Installation: %v", err)
		}

		httpClient = &http.Client{
			Transport: itr,
		}
	} else if githubToken != "" {
		src := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: githubToken},
		)
		httpClient = oauth2.NewClient(ctx, src)
	} else {
		return nil, nil, fmt.Errorf("expected the GITHUB_APP_ID and GITHUB_APP_KEY environment variables to be set, but they weren't. Nor was the GITHUB_TOKEN environment variable set as a fallback")
	}

	client := github.NewClient(httpClient)
	gqlClient := githubv4.NewClient(httpClient)

	return client, gqlClient, nil
}

func openDB(path string) (*sql.DB, error) {
	return sql.Open("sqlite", path)
}

func handleDBInit(ctx context.Context, path string) error {
	sqlDB, err := openDB(path)
	if err != nil {
		return fmt.Errorf("failed to open database at %v for initialising: %v", path, err)
	}

	_, err = sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	if err != nil {
		return fmt.Errorf("failed to initialise database with schema at %v: %v", path, err)
	}

	return nil
}

func handleDiscoverGitHub(ctx context.Context, path string, org string) error {
	sqlDB, err := openDB(path)
	if err != nil {
		return fmt.Errorf("failed to open database at %v for persisting: %v", path, err)
	}

	_, gqlClient, err := newGitHubClients(ctx, os.Getenv("GITHUB_TOKEN"), os.Getenv("GITHUB_APP_ID"), os.Getenv("GITHUB_APP_KEY"), os.Getenv("GITHUB_APP_INSTALLATION_ID"))
	if err != nil {
		return err
	}

	fmt.Printf("Discovering repositories under the %v org\n", org)
	repos, err := gh.GetReposForOrg(ctx, gqlClient, org)
	if err != nil {
		return err
	}

	pw := progress.NewWriter()
	go pw.Render()

	configTracker := &progress.Tracker{
		Message: fmt.Sprintf("Attempting to retrieve Renovate configuration for %d repos", len(repos)),
		Total:   int64(len(repos)),
	}
	pw.AppendTracker(configTracker)

	var allContents []domain.Contents
	for _, rk := range repos {
		contents, err := gh.RetrieveRenovateConfiguration(ctx, gqlClient, rk.Organisation, rk.Repo)
		if err != nil {
			configTracker.MarkAsErrored()
			// TODO handle `*github.AbuseRateLimitError`
			fmt.Println(err)
		} else if contents != nil {
			allContents = append(allContents, *contents)
		}
		configTracker.Increment(1)
	}

	configTracker.MarkAsDone()

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to create transaction: %v", err)
	}
	defer tx.Rollback()

	dbTracker := &progress.Tracker{
		Message: fmt.Sprintf("Persisting %d repos' Renovate configuration", len(allContents)),
		Total:   int64(len(allContents)),
	}
	pw.AppendTracker(configTracker)

	q := db.New(sqlDB)
	for _, c := range allContents {
		err = q.InsertRow(ctx, db.InsertRowParams{
			Platform:     c.Key.Platform,
			Organisation: c.Key.Organisation,
			Repo:         c.Key.Repo,
			ConfigPath:   c.ConfigPath,
			Config:       c.RawConfig,
		})
		if err != nil {
			return err
		}
		dbTracker.Increment(1)
	}
	dbTracker.MarkAsDone()

	return tx.Commit()
}
